<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\ShortLink;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class ShortLinkController extends Controller
{
    /**
     * It is used to show the resource list.
     *
     * @return View|\Illuminate\Foundation\Application|Factory|Application
     */
    public function index(): View|\Illuminate\Foundation\Application|Factory|Application
    {
        $shortLinks = ShortLink::latest()->get();

        return view('shortenLink', compact('shortLinks'));
    }

    /**
     * It is used to show the resource list.
     *
     * @param Request $request
     * @return \Illuminate\Foundation\Application|Redirector|RedirectResponse|Application
     */
    public function store(Request $request): \Illuminate\Foundation\Application|Redirector|RedirectResponse|Application
    {
        $request->validate([
            'link' => 'required|url'
        ]);

        ShortLink::query()->create([
            'link' => $request->link,
            'code' => substr(md5((string)mt_rand()), 0, 7)
        ])->save();

        return redirect('generate-shorten-link')
            ->with('success', 'Shorten Link Generated Successfully!');
    }

    /**
     * It is used to show the resource list.
     *
     * @param $code
     * @return \Illuminate\Foundation\Application|Redirector|RedirectResponse|Application
     */
    public function shortenLink($code): \Illuminate\Foundation\Application|Redirector|RedirectResponse|Application
    {
        $find = ShortLink::where('code', $code)->first();

        return redirect($find->link);
    }
}
