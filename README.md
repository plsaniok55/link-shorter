### Project initialization

1. Firstly, you should prepare your environment if needed. You can use Docker or Warden environment. If you don't want
   to do it you can use this command after step 2: php artisan serve;
2. Then, you need to install base project files. You should use this command: composer create-project laravel/laravel
   example-app;
3. Now, download git files with the following commands:
    1. cd example-app
    2. git remote add origin https://gitlab.com/plsaniok55/link-shorter.git
    3. git pull origin main
4. Run command ```php artisan migrate```

### Operation of the project

Open the link your-link/generate-shorten-link. Now you can try to create shorter links. For the test you can use these
are:
 - https://your-link/test - valid URL
 - https://your-link/welcomeb - invalid URL

## Thanks for reviewing!!!
